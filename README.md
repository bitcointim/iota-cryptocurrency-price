Here is a list of cryptocurrency price and price charts for IOTA

- https://www.coingecko.com/en/coins/iota/
- https://www.coingecko.com/en/price_charts/iota/btc
- https://www.coingecko.com/en/price_charts/iota/usd
- https://www.coingecko.com/en/price_charts/iota/eur
- https://www.coingecko.com/en/price_charts/iota/jpy
- https://www.coingecko.com/en/price_charts/iota/chf

